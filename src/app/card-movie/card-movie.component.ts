import { Component, OnDestroy, OnInit } from '@angular/core';
import { Movie } from '../shared/movie.model';
import { MovieService } from '../shared/movie.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-card-movie',
  templateUrl: './card-movie.component.html',
  styleUrls: ['./card-movie.component.css']
})
export class CardMovieComponent implements OnInit , OnDestroy {
  movies!: Movie[];

  moviesChangeSubscription!: Subscription;
  moviesFetchingSubscription!: Subscription;
  moviesDeletingSubscription!: Subscription;
  isFetching: boolean = false;
  isDeleting: boolean = false;


  constructor(private movieService: MovieService) {}

  ngOnInit(): void {
    this.movies = this.movieService.getMovies();
    this.moviesChangeSubscription =
    this.movieService.moviesChange.subscribe((movies: Movie[]) => {
      this.movies = movies;
    });
    this.moviesDeletingSubscription =
      this.movieService.movieDeleting.subscribe((isDeleting: boolean) => {
        this.isDeleting = isDeleting;
      });
    this.moviesFetchingSubscription =
      this.movieService.moviesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.movieService.fetchMovies();
  }

  deleteMovie(id: string) {
    this.movieService.deleteMovie(id).subscribe(() => {
      this.movieService.fetchMovies();
    });
  }

  ngOnDestroy() {
    this.moviesChangeSubscription.unsubscribe();
    this.moviesFetchingSubscription.unsubscribe();
    this.moviesDeletingSubscription.unsubscribe();
  }
}
