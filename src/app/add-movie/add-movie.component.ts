import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MovieService } from '../shared/movie.service';
import { Movie } from '../shared/movie.model';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit, OnDestroy {
  @ViewChild('nameInput') nameInput!: ElementRef;

  isUpLoading = false;
  movieUploadingSubscription!: Subscription;

  constructor(private http: HttpClient, private movieService: MovieService ) { }

  ngOnInit() {
    this.movieUploadingSubscription = this.movieService.movieUploading.subscribe((isUploading:boolean) => {
      this.isUpLoading = isUploading;
    });
  }

  addMovie() {
    const name: string = this.nameInput.nativeElement.value;
    const id = Math.random().toString();
    const movie = new Movie(id, name);

    this.movieService.addMovie(movie).subscribe(() => {
      this.movieService.fetchMovies();
    });
  }

  ngOnDestroy() {
    this.movieUploadingSubscription.unsubscribe();
  }
}
