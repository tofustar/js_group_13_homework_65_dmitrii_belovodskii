import { Movie } from './movie.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()

export class MovieService {

  moviesChange = new Subject<Movie[]>();
  moviesFetching = new Subject<boolean>();
  movieUploading = new Subject<boolean>();
  movieDeleting = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  private movies: Movie[] = [];

  fetchMovies() {
    this.moviesFetching.next(true);
    this.http.get<{ [id: string]: Movie }>('https://js-group-13-default-rtdb.firebaseio.com/movies.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const movieDate = result[id];
          return new Movie(id, movieDate.name)
        });
      }))
      .subscribe(movies => {
        this.movies = movies;
        this.moviesChange.next(this.movies.slice());
        this.moviesFetching.next(false);
      }, () => {
        this.moviesFetching.next(false);
      })
  }

  getMovies() {
    return this.movies.slice();
  }

  addMovie(movie: Movie) {
    const body = {
      name: movie.name,
    };

    this.movieUploading.next(true);

    return this.http.post(`https://js-group-13-default-rtdb.firebaseio.com/movies.json`, body).pipe(
      tap(() => {
          this.movieUploading.next(false);
        }, () => {
          this.movieUploading.next(false);
        }
      ),
    );
  }

  deleteMovie(id: string) {
    this.movieDeleting.next(true);
    return this.http.delete<Movie>(`https://js-group-13-default-rtdb.firebaseio.com/movies/` + id + `.json`).pipe(
      tap(() => {
        this.movieDeleting.next(false);
      }, () => {
        this.movieDeleting.next(false);
      })
    );
  }

}
