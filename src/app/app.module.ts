import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { CardMovieComponent } from './card-movie/card-movie.component';
import { HttpClientModule } from '@angular/common/http';
import { MovieService } from './shared/movie.service';

@NgModule({
  declarations: [
    AppComponent,
    AddMovieComponent,
    CardMovieComponent
  ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule
    ],
  providers: [MovieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
